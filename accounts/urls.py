from django.urls import path
from accounts.views import after_login, user_logout, signup

urlpatterns = [
    path("login/", after_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
