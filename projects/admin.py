from django.contrib import admin
from projects.models import Project
from tasks.models import Task


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ("name",)
