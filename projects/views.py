from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    member_list = list.exists()
    context = {"list_projects": list, "list_member_projects": member_list}
    return render(request, "list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    return render(request, "detail.html", {"project": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "project_create.html", context)
